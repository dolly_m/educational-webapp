//canvas1
   var toggleColor = (function(){
        $("#expr1").show();
        $("#expr2, #expr3, #expr4").hide();
    });
   var toggleColor2 = (function(){
        $("#expr2").show();
        $("#expr1, #expr3, #expr4").hide();     
});
   var toggleColor3 = (function(){
        $("#expr3").show();
        $("#expr2, #expr1, #expr4").hide();  
});
   var toggleColor4 = (function(){
        $("#expr4").show();
        $("#expr1, #expr2, #expr3").hide();  
});
   var toggleColor5 = (function(){
        $("#vennCanvas1").css('background', 'white');
   });

//canvas2
   var toggleColor6 = (function(){
        $("#expr5").show();
        $("#expr6, #expr7, #expr8, #expr9, #expr10, #expr11, #expr12").hide();
    });
   var toggleColor7 = (function(){
        $("#expr6").show();
        $("#expr5, #expr7, #expr8, #expr9, #expr10, #expr11, #expr12").hide();     
});
   var toggleColor8 = (function(){
        $("#expr7").show();
        $("#expr5, #expr6, #expr8, #expr9, #expr10, #expr11, #expr12").hide();  
});
   var toggleColor9 = (function(){
        $("#expr8").show();
        $("#expr5, #expr6, #expr7, #expr9, #expr10, #expr11, #expr12").hide();  
});
   var toggleColor10 = (function(){
        $("#expr9").show();
        $("#expr5, #expr7, #expr8, #expr6, #expr10, #expr11, #expr12").hide();
    });
   var toggleColor11 = (function(){
        $("#expr10").show();
        $("#expr5, #expr7, #expr8, #expr9, #expr6, #expr11, #expr12").hide();    
});
   var toggleColor12 = (function(){
        $("#expr11").show();
        $("#expr5, #expr7, #expr8, #expr9, #expr10, #expr6, #expr12").hide(); 
});
    var toggleColor14 = (function(){
        $("#expr12").show();
        $("#expr5, #expr7, #expr8, #expr9, #expr10, #expr6, #expr11").hide(); 
   });
    var toggleColor13 = (function(){
        $("#vennCanvas2").css('background', 'white');
   });


function commonFunc(thePath,d){
  d3.event.stopPropagation();
  d3.select(thePath).style('fill', d.color);
}

//2 circle
var data=[
{
    toggleFunc: function(d){
       toggleColor2(1);
       toggleColor5(1);
       d3.selectAll("path#path2, path#path3").style('fill', 'white');
    },
    color: 'magenta'
  }
];

var data2=[
{
    toggleFunc2: function(d){
       toggleColor(1);
       toggleColor5(2);
       d3.selectAll("path#path1, path#path3").style('fill', 'white');
    },
    color: 'magenta'
  }
];

var data3=[
{
    toggleFunc3: function(d){
       toggleColor3(1);
       toggleColor5(3);
       d3.selectAll("path#path1, path#path2").style('fill', 'white');
    },
    color: 'magenta'
  }
];

//Render 2-Circle Venn Diagram
var wholeV = d3.select('.v1');
var wholeVCanvas = wholeV.append("svg").attr("width", 200).attr("height", 190);

wholeVCanvas.data(data)
  .append("svg:path")
  .attr("d", "M100 15 A 60 60, 0, 0, 0, 100 115 A 60 60, 0, 1, 1, 100 15")
  .attr("id", 'path1')
  .attr("class", 'vennPaths')
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc(d);
    clicky.goal('Clicked2');
  }); 

wholeVCanvas.data(data2)
  .append("svg:path")
  .attr("d", "M100 15 A 60 60, 0, 0, 1, 100 115 A 60 60, 0, 0, 1, 100 15")
  .attr("id", 'path2')
  .attr("class", 'vennPaths')
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc2(d);
    clicky.goal('Clicked2');
  }); 

wholeVCanvas.data(data3)
  .append("svg:path")
  .attr("d", "M100 15 A 60 60, 0, 0, 1, 100 115 A 60 60, 0, 1, 0, 100 15")
  .attr("id", 'path3')
  .attr("class", 'vennPaths')
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc3(d);
    clicky.goal('Clicked2');
  }); 

$("#vennCanvas1").click(function(){
    $("#vennCanvas1").css('background', 'magenta');
    d3.selectAll(".vennPaths").style('fill', 'white');
    toggleColor4(1);
    clicky.goal('Clicked2');  
});


//3 Circle
var data4=[
{
    toggleFunc4: function(d){
       toggleColor11(1);
       toggleColor13(1);
       d3.selectAll("path#path5, path#path6, path#path7, path#path8, path#path9, path#path10").style('fill', 'white');
    },
    color: 'magenta'
  }
];

var data5=[
{
    toggleFunc5: function(d){
       toggleColor10(1);
       toggleColor13(2);
       d3.selectAll("path#path4, path#path6, path#path7, path#path8, path#path9, path#path10").style('fill', 'white');
    },
    color: 'magenta'
  }
];

var data6=[
{
    toggleFunc6: function(d){
       toggleColor6(1);
       toggleColor13(3);
       d3.selectAll("path#path4, path#path5, path#path7, path#path8, path#path9, path#path10").style('fill', 'white');
    },
    color: 'magenta'
  }
];

var data7=[
{
    toggleFunc7: function(d){
       toggleColor7(1);
       toggleColor13(4);
       d3.selectAll("path#path4, path#path6, path#path5, path#path8, path#path9, path#path10").style('fill', 'white');
    },
    color: 'magenta'
  }
];

var data8=[
{
    toggleFunc8: function(d){
       toggleColor8(1);
       toggleColor13(5);
       d3.selectAll("path#path4, path#path6, path#path7, path#path5, path#path9, path#path10").style('fill', 'white');
    },
    color: 'magenta'
  }
];

var data9=[
{
    toggleFunc9: function(d){
       toggleColor12(1);
       toggleColor13(6);
       d3.selectAll("path#path4, path#path6, path#path7, path#path8, path#path5, path#path10").style('fill', 'white');
    },
    color: 'magenta'
  }
];

var data10=[
{
    toggleFunc10: function(d){
       toggleColor9(1);
       toggleColor13(7);
       d3.selectAll("path#path4, path#path6, path#path7, path#path8, path#path9, path#path5").style('fill', 'white');
    },
    color: 'magenta'
  }
];


//Render 3-Circle Venn Diagram
var wholeV2 = d3.select('.v2');
var wholeV2Canvas = wholeV2.append("svg").attr("width", 200).attr("height", 190);

wholeV2Canvas.data(data4)
  .append("svg:path")
  .attr("d", "M100 15 A 54 50, 0, 1, 0, 40 119 A 70 70, 0, 0, 1, 73 60 A 55 55, 0, 0, 1, 100 15")
  .attr("id", 'path4')
  .attr("class", 'vennPaths2')
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc4(d);
    clicky.goal('Clicked3');
  }); 

wholeV2Canvas.data(data5)
  .append("svg:path")
  .attr("d", "M100 15 A 55 55, 0, 0, 0, 73 61 A 55 55, 0, 0, 1, 127 61 A 64 64, 0, 0, 0, 100 15")
  .attr("id", 'path5')
  .attr("class", 'vennPaths2')
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc5(d);
    clicky.goal('Clicked3');
  }); 

wholeV2Canvas.data(data6)
  .append("svg:path")
  .attr("d", "M73 61 A 55 55, 0, 0, 1, 127 61 A 60 60, 0, 0, 1, 100 115 A 60 60, 0, 0, 1, 73 61")
  .attr("id", 'path6')
  .attr("class", 'vennPaths2')
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc6(d);
    clicky.goal('Clicked3');
  }); 

wholeV2Canvas.data(data7)
  .append("svg:path")
  .attr("d", "M73 61 A 60 60, 0, 0, 0, 100 115 A 60 60, 0, 0, 1, 40 119 A 60 60, 0, 0, 1, 73 61")
  .attr("id", 'path7')
  .attr("class", 'vennPaths2')
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc7(d);
    clicky.goal('Clicked3');
  }); 

wholeV2Canvas.data(data8)
  .append("svg:path")
  .attr("d", "M127 61 A 60 60, 0, 0, 1, 100 115 A 60 60, 0, 0, 0, 160 118 A 60 60, 0, 0, 0, 127 61")
  .attr("id", 'path8')
  .attr("class", 'vennPaths2')
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc8(d);
    clicky.goal('Clicked3');
  }); 

wholeV2Canvas.data(data9)
  .append("svg:path")
  .attr("d", "M127 61 A 60 60, 0, 0, 1, 160 118 A 10 10, 0, 0, 0, 100 15 A 60 60, 0, 0, 1, 127 61")
  .attr("id", 'path9')
  .attr("class", 'vennPaths2')
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc9(d);
    clicky.goal('Clicked3');
  }); 

wholeV2Canvas.data(data10)
  .append("svg:path")
  .attr("d", "M100 115 A 60 60, 0, 0, 1, 40 119 A 55 55, 0, 0, 0 160 119 A 60 60, 0, 0, 1, 100 115")
  .attr("id", 'path10')
  .attr("class", 'vennPaths2')
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc10(d);
    clicky.goal('Clicked3');
  }); 

$("#vennCanvas2").click(function(){
    $("#vennCanvas2").css('background', 'magenta');
    d3.selectAll(".vennPaths2").style('fill', 'white');
    toggleColor14(1);
    clicky.goal('Clicked3');
    
});
