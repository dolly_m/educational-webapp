

function checkAnswer(ticks, crosses, correct) {

  // check each id with a tick, to see if they are ALL visible
  var isCorrect = ticks.every(function(id) {
    return $(id).is(':visible')
  });

  function hideTicks(){
    for (var i = 0; i < ticks.length; i++){
        $(ticks[i]).hide();
    }
  }
  function hideCrosses(){
    for (var j = 0; j < crosses.length; j++){
        $(crosses[j]).hide();
    }
  }

  // if they were all visible, then hide the ticks and crosses and show the correct box
  if(isCorrect) {
    hideTicks();
    hideCrosses();
    $(correct).show();
    
  }
   if($(correct).is(':visible')){
   hideTicks();
   hideCrosses();
  }  
}


function checkAns() {
    checkAnswer(['#div1', '#div2'], ['#div5', '#div6'], '#div4');
  };
function checkAns2() {
    checkAnswer(['#div7', '#div8'], ['#div11', '#div12'], '#div10');
  };
function checkAns3() {
    checkAnswer(['#div13'], ['#div18', '#div19'], '#div17');
  };
function checkAns4() {
    checkAnswer(['#div1', '#div2', '#div3'], ['#div5'], '#div4');
  };
function checkAns5() {
    checkAnswer(['#div13', '#div14', '#div15', '#div16'], [''], '#div17');
  };
function checkAns6() {
    checkAnswer(['#div1'], ['#div5', '#div6'], '#div4');
  };
function checkAns7() {
    checkAnswer(['#div7'], ['#div11', '#div12'], '#div10');
  };
function checkAns8() {
    checkAnswer(['#div13', '#div14'], ['#div18', '#div19'], '#div17');
  };
function checkAns9() {
    checkAnswer(['#div7', '#div8', '#div9'], ['#div11'], '#div10');
  };
function checkAns10() {
    checkAnswer(['#div13', '#div14', '#div15'], ['#div18'], '#div17');
  };




var totalQuestions = $('.questions').size();
var currentQuestion = 0;
$questions = $('.questions');
$questions.hide();
$($questions.get(currentQuestion)).fadeIn();
$('#next').click(function(){
  $($questions.get(currentQuestion)).fadeOut(function(){
    currentQuestion = currentQuestion + 1;
    $(".correct, .right, .wrong, .label").hide();
    if(currentQuestion == totalQuestions){
      //alert('You have completed the quiz! Try the 3-Circle one?');
      $("#dialog").dialog();
      $('#next').hide();
    } else {
      $($questions.get(currentQuestion)).fadeIn();
    }
  });
});


$('#hint').click(function(){
    $(this).hide();
    $("#para").hide();
    $("#vennCanvas1").show();
    $("#vennCanvas2").show();
    $(".label").show();
    $("#para1").show();
    $("#para2").show();
    $("#para3").show();
    $("#para4").show();
    clicky.goal('Clicked Hint');
});

$('#hint2').click(function(){
    $(this).hide();
    $("#para5").hide();
    $("#vennCanvas4").show();
    $("#vennCanvas5").show();
    $(".label").show();
    $("#para6").show();
    $("#para7").show();
    $("#para8").show();
    $("#para9").show();
    clicky.goal('Clicked Hint2');
});

$('#hint3').click(function(){
    $(this).hide();
    $("#para10").hide();
    $("#vennCanvas7").show();
    $("#vennCanvas8").show();
    $(".label").show();
    $("#para11").show();
    $("#para12").show();
    $("#para13").show();
    $("#para14").show();
    clicky.goal('Clicked Hint3');
});

$('#hint4').click(function(){
    $(this).hide();
    $("#para15").hide();
    $("#vennCanvas10").show();
    $("#vennCanvas11").show();
    $(".label").show();
    $("#para16").show();
    $("#para17").show();
    $("#para18").show();
    $("#para19").show();
    clicky.goal('Clicked Hint4');
});

$('#hint5').click(function(){
    $(this).hide();
    $("#para20").hide();
    $("#vennCanvas13").show();
    $("#vennCanvas14").show();
    $(".label").show();
    $("#para21").show();
    $("#para22").show();
    $("#para23").show();
    $("#para24").show();
    clicky.goal('Clicked Hint5');
});

$('#hint6').click(function(){
    $(this).hide();
    $("#para25").hide();
    $("#vennCanvas16").show();
    $("#vennCanvas17").show();
    $(".label").show();
    $("#para26").show();
    $("#para27").show();
    $("#para28").show();
    $("#para29").show();
    clicky.goal('Clicked Hint6');
});

$('#hint7').click(function(){
    $(this).hide();
    $("#para30").hide();
    $("#vennCanvas19").show();
    $("#vennCanvas20").show();
    $(".label").show();
    $("#para31").show();
    $("#para32").show();
    $("#para33").show();
    $("#para34").show();
    clicky.goal('Clicked Hint7');
});

//canvas1
   var toggleColor = (function(id){
        $("#div1").show();
        $(".wrong").hide();
    });
   var toggleColor2 = (function(id2){
        $("#div2").show();
        $(".wrong").hide();       
});
   var toggleColor3 = (function(id3){
        $("#div3").show();
        $(".wrong").hide();   
});
   var toggleColor4 = (function(id4){
        $("#div5").show().delay(2500).hide(0); 
        clicky.goal('Clicked wrong area'); 
});
   var toggleColor5 = (function(id5){
        $("#div6").show().delay(2500).hide(0);
        clicky.goal('Clicked wrong area');  
});


//canvas 2
   var toggleColor6 = (function(id6){
        $("#div7").show();
        $(".wrong").hide();  
});
   var toggleColor7 = (function(id7){
        $("#div8").show();
        $(".wrong").hide();
});
   var toggleColor8 = (function(id8){
        $("#div9").show();
        $(".wrong").hide();
});
   var toggleColor9 = (function(id9){
        $("#div11").show().delay(2500).hide(0);
        clicky.goal('Clicked wrong area');    
         
});
    var toggleColor10 = (function(id10){
        $("#div12").show().delay(2500).hide(0);
        clicky.goal('Clicked wrong area');     
});
   
//canvas 3
    var toggleColor11 = (function(id11){
        $("#div13").show(); 
        $(".wrong").hide(); 
});
    var toggleColor12 = (function(id12){
        $("#div14").show(); 
        $(".wrong").hide(); 
});
   var toggleColor13 = (function(id13){
        $("#div15").show(); 
        $(".wrong").hide(); 
});
   var toggleColor14 = (function(id14){
        $("#div16").show();
        $(".wrong").hide();   
});
   var toggleColor15 = (function(id15){
        $("#div18").show().delay(2500).hide(0);
        clicky.goal('Clicked wrong area');  
});
   var toggleColor16 = (function(id16){
        $("#div19").show().delay(2500).hide(0);
        clicky.goal('Clicked wrong area'); 
});


//------------------------------xxxxxxxxxxxxx------------------


function commonFunc(thePath,d){
  d3.event.stopPropagation();
  d3.select(thePath).style('fill', d.color);
}


var data = [
  {
    toggleFunc: function(d){
       toggleColor4(1);
       checkAns(1);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
       toggleColor6(1);
       checkAns2(1);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
       toggleColor15(1);
       checkAns3(1);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
       toggleColor(2);
       checkAns4(1);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
       toggleColor6(2);
       checkAns2(5);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
       toggleColor11(2);
       checkAns5(1);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
       toggleColor(3);
       checkAns4(5);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
       toggleColor9(3);
       checkAns2(9);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
       toggleColor15(3);
       checkAns3(5);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
       toggleColor4(4);
       checkAns6(1);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
       toggleColor9(4);
       checkAns7(1);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
       toggleColor15(5);
       checkAns8(1);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
       toggleColor(5);
       checkAns(5);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
       toggleColor6(5);
       checkAns9(1);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
       toggleColor11(5);
       checkAns10(1);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
       toggleColor4(7);
       checkAns6(5);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
       toggleColor6(6);
       checkAns7(5);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
       toggleColor11(6);
       checkAns8(5);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
       toggleColor4(8);
       checkAns6(9);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
       toggleColor9(9);
       checkAns7(9);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
       toggleColor15(8);
       checkAns8(9);
    },
    color: 'white'
  }
];

var data2 = [
  {
    toggleFunc2: function(d){
       toggleColor5(1);
       checkAns(2);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
       toggleColor9(1);
       checkAns2(2);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
       toggleColor16(1);
       checkAns3(2);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
       toggleColor4(2);
       checkAns4(2);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
       toggleColor7(2);
       checkAns2(6);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
       toggleColor12(1);
       checkAns5(2);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
       toggleColor4(3);
       checkAns4(6);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
       toggleColor6(3);
       checkAns2(10);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
       toggleColor16(2);
       checkAns3(6);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
       toggleColor5(2);
       checkAns6(2);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
       toggleColor6(4);
       checkAns7(2);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
       toggleColor11(4);
       checkAns8(2);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
       toggleColor2(4);
       checkAns(6);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
       toggleColor7(4);
       checkAns9(2);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
       toggleColor12(3);
       checkAns10(2);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
       toggleColor5(4);
       checkAns6(6);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
       toggleColor9(7);
       checkAns7(5);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
       toggleColor15(7);
       checkAns8(6);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
       toggleColor5(6);
       checkAns6(10);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
       toggleColor6(7);
       checkAns7(10);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
       toggleColor11(7);
       checkAns8(10);
    },
    color: 'magenta'
  }
];

var data3=[ 
    {
    toggleFunc3: function(d){
       toggleColor(1);
       checkAns(3);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
       toggleColor10(1);
       checkAns2(3);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
       toggleColor15(2);
       checkAns3(3);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
       toggleColor2(2);
       checkAns4(3);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
       toggleColor9(2);
       checkAns2(7);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
       toggleColor13(1);
       checkAns5(3);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
       toggleColor2(3);
       checkAns4(7);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
       toggleColor7(3);
       checkAns2(11);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
       toggleColor11(3);
       checkAns3(7);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
       toggleColor4(5);
       checkAns6(3);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
       toggleColor10(4);
       checkAns7(3);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
       toggleColor16(3);
       checkAns8(3);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
       toggleColor4(6);
       checkAns(7);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
       toggleColor8(1);
       checkAns9(3);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
       toggleColor13(2);
       checkAns10(3);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
       toggleColor(6);
       checkAns6(7);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
       toggleColor10(5);
       checkAns7(7);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
       toggleColor12(4);
       checkAns8(7);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
       toggleColor4(9);
       checkAns6(11);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
       toggleColor9(10);
       checkAns7(11);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
       toggleColor16(5);
       checkAns8(11);
    },
    color: 'white'
  }
  ];






//Render Venn Diagrams
var wholeV = d3.selectAll('.v1, .v2, .v3');
var wholeVCanvas = wholeV.append("svg").attr("width", 200).attr("height", 190);

wholeVCanvas.data(data)
  .append("svg:path")
  .attr("d", "M100 15 A 60 60, 0, 0, 0, 100 115 A 60 60, 0, 1, 1, 100 15")
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc(d);
  }); 

wholeVCanvas.data(data2)
  .append("svg:path")
  .attr("d", "M100 15 A 60 60, 0, 0, 1, 100 115 A 60 60, 0, 0, 1, 100 15")
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc2(d);
  }); 

wholeVCanvas.data(data3)
  .append("svg:path")
  .attr("d", "M100 15 A 60 60, 0, 0, 1, 100 115 A 60 60, 0, 1, 0, 100 15")
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc3(d);
  }); 




//--------------------------------xxxxxxx-----------------------

    
$("#vennCanvas1").click(function(){
    $("#vennCanvas1").css('background', 'magenta');
    toggleColor2(1);
    checkAns(4);
});

$("#vennCanvas2").click(function(){
    $("#vennCanvas2").css('background', 'magenta');
    toggleColor7(1);
    checkAns2(4);
});
    
$("#vennCanvas3").click(function(){
    $("#vennCanvas3").css('background', 'magenta');
    toggleColor11(1);
    checkAns3(4);
});

$("#vennCanvas4").click(function(){
    $("#vennCanvas4").css('background', 'magenta');
    toggleColor3(1);
    checkAns4(4);
});

$("#vennCanvas5").click(function(){
    toggleColor10(2);
    checkAns2(8);
    clicky.goal('Clicked wrong area');
});

$("#vennCanvas6").click(function(){
    $("#vennCanvas6").css('background', 'magenta');
    toggleColor14(1);
    checkAns5(4);
});

$("#vennCanvas7").click(function(){
    $("#vennCanvas7").css('background', 'magenta');
    toggleColor3(2);
    checkAns4(8);
});

$("#vennCanvas8").click(function(){
    toggleColor10(3);
    checkAns2(12);
    clicky.goal('Clicked wrong area');
});

$("#vennCanvas9").click(function(){
    toggleColor15(4);
    checkAns3(8);
    clicky.goal('Clicked wrong area');
});

$("#vennCanvas10").click(function(){
    $("#vennCanvas10").css('background', 'magenta');
    toggleColor(4);
    checkAns6(4);
});

$("#vennCanvas11").click(function(){
    toggleColor9(5);
    checkAns7(4);
    clicky.goal('Clicked wrong area');
});

$("#vennCanvas12").click(function(){
   $("#vennCanvas12").css('background', 'magenta');
    toggleColor12(2);
    checkAns8(4);
});

$("#vennCanvas13").click(function(){
    toggleColor5(3);
    checkAns(8);
    clicky.goal('Clicked wrong area');
});

$("#vennCanvas14").click(function(){
    toggleColor9(6);
    checkAns9(4);
    clicky.goal('Clicked wrong area');
});

$("#vennCanvas15").click(function(){
    toggleColor15(6);
    checkAns10(4);
    clicky.goal('Clicked wrong area');
});

$("#vennCanvas16").click(function(){
    toggleColor5(5);
    checkAns6(8);
    clicky.goal('Clicked wrong area');
});

$("#vennCanvas17").click(function(){
    toggleColor9(8);
    checkAns7(8);
    clicky.goal('Clicked wrong area');
});

$("#vennCanvas18").click(function(){
    toggleColor16(4);
    checkAns8(8);
    clicky.goal('Clicked wrong area');
});

$("#vennCanvas19").click(function(){
   $("#vennCanvas19").css('background', 'magenta');
    toggleColor(7);
    checkAns6(12);
});

$("#vennCanvas20").click(function(){
    toggleColor10(6);
    checkAns7(12);
    clicky.goal('Clicked wrong area');
});

$("#vennCanvas21").click(function(){
   $("#vennCanvas21").css('background', 'magenta');
    toggleColor12(5);
    checkAns8(12);
});

