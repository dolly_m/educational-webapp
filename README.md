# README #

An Educational Webapp to help users learn some concepts of Logic. Learn the relationship between 2-Circle, 3-Circle Venn Diagrams and Boolean Algebra through some quizzes. There's also an introductory tool. At the end, if you like, you could fill in the survey as well. 

Web technologies used: HTML, CSS, Javascript, jQuery, D3.js, Click Analytics, SurveyMonkey

### How do I get set up? ###

* Once you've downloaded a copy of the repository on your local machine, launch 'index.html' and the project will be loaded
* Alternatively visit [this page](http://homepages.inf.ed.ac.uk/s1241360/) as it has been hosted here.

### COPYRIGHT ###
Please contact me if you would like to use this code. 